package com.example.tpspringmongodbangular.services;

import com.example.tpspringmongodbangular.entities.Employee;
import com.example.tpspringmongodbangular.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository _employeeRepository;

    public Employee create(Employee employee) {
        return _employeeRepository.save(employee);
    }

    public Employee getEmployeeById(String id) throws Exception {
        if (_employeeRepository.findById(id).isPresent())
            return _employeeRepository.findById(id).get();
        else
            throw new Exception("Employee with id : " + id + " doesn't exist.");
    }

    public List<Employee> getEmployees() {
        return _employeeRepository.findAll();
    }

    public Employee updateEmployee(Employee employee) {
        return _employeeRepository.save(employee);
    }

    public String deleteEmployeeById(String id) throws Exception {
        try {
            _employeeRepository.deleteById(id);
            return "Deleted employee.";
        } catch (Exception e) {
            throw new Exception("Error during deletion.");
        }
    }

    public String deleteEmployees() {
        _employeeRepository.deleteAll();
        return "All employees have been removed.";
    }
}
