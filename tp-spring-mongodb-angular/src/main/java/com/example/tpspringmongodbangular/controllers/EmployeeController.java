package com.example.tpspringmongodbangular.controllers;

import com.example.tpspringmongodbangular.entities.Employee;
import com.example.tpspringmongodbangular.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/employee")
@CrossOrigin(origins = {"*"}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class EmployeeController {

    @Autowired
    private EmployeeService _employeeService;

    @PostMapping()
    public ResponseEntity<Employee> post(@RequestBody Employee employee) {
        return ResponseEntity.ok(_employeeService.create(employee));
    }

    @GetMapping()
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.ok(_employeeService.getEmployees());
    }

    @GetMapping("{id}")
    public ResponseEntity<Employee> getById(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(_employeeService.getEmployeeById(id));
    }

    @PutMapping("")
    public ResponseEntity<Employee> put(@RequestBody Employee employee) {
        return ResponseEntity.ok(_employeeService.updateEmployee(employee));
    }

    @DeleteMapping()
    public ResponseEntity<String> deleteAll() {
        return ResponseEntity.ok(_employeeService.deleteEmployees());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteById(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(_employeeService.deleteEmployeeById(id));
    }
}
