package com.example.tpspringmongodbangular.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@Document(collection = "employees")
public class Employee {

    @Id
    private String id;
    private String firstname;
    private String lastname;
    private Poste poste;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDate;
    private Double salary;
}
