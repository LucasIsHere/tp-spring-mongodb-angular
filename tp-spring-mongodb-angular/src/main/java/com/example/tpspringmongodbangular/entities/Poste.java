package com.example.tpspringmongodbangular.entities;

public enum Poste {

    CEO("CEO"),
    SoftwareEngineer("Software engineer"),
    SeniorJSDeveloper("Senior JS developer"),
    IntegrationSpecialist("Integration specialist");

    private String poste;

    Poste(String poste) {
        this.poste = poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getPoste() {
        return poste;
    }
}
