package com.example.tpspringmongodbangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSpringMongodbAngularApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpSpringMongodbAngularApplication.class, args);
    }

}
